<?php
/*
Plugin Name: WP FlipBox ShortCode
Plugin URI:
Description: Flip flap effect for content box. The front and back boxes content are based on the WordPress contents. The FlipBox is used with a ShortCode.
Author: David Réchatin - Openscop
Version: 1.0
Author URI: http://www.openscop.fr
License:  AGPLv3
License URI: https://www.gnu.org/licenses/agpl-3.0.html
*/


function openscopAddHeader()
{
    wp_enqueue_script('os_script', plugins_url('/wp-flipbox-shortcode.js', __FILE__), array('jquery'));
    wp_enqueue_style('os_style', plugins_url('/wp-flipbox-shortcode.css', __FILE__));
}

add_action('wp_head', 'openscopAddHeader');


function insert_flipbox_shortcode($atts, $content = null)
{
    /*
     * use with shortcode :
     * [insert_flipbox front_content_id="4" back_content_id="473" box_style="" front_class="" front_style="background:green;" back_class="" back_style="background:blue;"]
    */
    extract(shortcode_atts(
            array(
                'front_content_id' => '',
                'back_content_id' => '',
                'box_style' => '',
                'front_class' => '',
                'front_style' => '',
                'back_class' => '',
                'back_style' => ''
            ), $atts)
    );


    $display = "<div class=\"flipbox $box_style\" style=\"$front_style\">";
    $display .= "<div class=\"front $front_class\">";
    $post = get_post($front_content_id);
    $display .= $post->post_content;
    $display .= '</div>';
    $display .= "<div class=\"back $back_class\" style=\"$back_style\">";
    $post = get_post($back_content_id);
    $display .= $post->post_content;
    $display .= '</div>';
    $display .= '</div>';
    return $display;
}

add_shortcode('insert_flipbox', 'insert_flipbox_shortcode');




?>